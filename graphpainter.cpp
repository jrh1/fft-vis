// Copywrite 2021 James Hemsing
// GPLv2 License
#include "graphpainter.h"
#include <QPainter>
#include <iostream>
#include "dataserver.h"
#include <QCoreApplication>
#include <QTime>
#include <QImage>

graphPainter::graphPainter(QWidget *parent) : QGraphicsView(parent)
{
    mDataServer = nullptr;
    mGraph = nullptr;

    QGraphicsScene * scene = new QGraphicsScene(this);
    setScene(scene);
    show();
}

void graphPainter::drawSpectrumToImage(float * floatData, int npts)
{
    int w = width();
    int h = height();
    QImage * new_img = new QImage(w, h, QImage::Format_Mono);
    static QPainter qp;
    //qp.setCompositionMode(QPainter::CompositionMode_Source);

    // Draw black frame

  //QPen black (Qt::black, 1, Qt::SolidLine);
  //qp.setPen(black);
  //qp.drawLine(0, 0, w, 0);
  //qp.drawLine(0, 0, 0, h);
  //qp.drawLine(w, 0, w-1, h);
  //qp.drawLine(0, h-1, w, h-1);
    QBrush br (Qt::black);
    qp.fillRect(QRect(0, 0, w, h), br);


    // Draw spectrum

    QPen pen (Qt::green, 1, Qt::SolidLine);
    qp.setPen(pen);

    if (!floatData) return;

    float step = (float)npts / (float)w;
    //if (step < 1.0) step = 1.0;
    //std::cout << "step: " << step << std::endl;


    // Search for min and max value while storing the dB compute results

    float hf = (float)h;
    float f;
    int i, mid;
    float max = -INFINITY;
    float min = INFINITY;
    for (i = 0; i < npts; i ++) {
        f = floatData[i];
        if (qIsInf(f)) // if any data are bad, skip it
            continue;
        if (f > max)
           max = f;
        if (f < min)
           min = f;
    }
    if ( fabs(min - max) < 0.01 ) return;

    float scale = hf/(max - min);

    //std::cout << "scale: " << scale << std::endl;

    QPointF a, b;
    a.setX(0);
    a.setY(hf);

    float acc, accm, j, k;
    j = 0.0;
    k = 0.0;
    acc = floatData[0] * scale;
    int x_pix = 0;

    for (i = 0; i < npts; i++) {
        if (i < npts/2) mid = npts/2;
        else            mid = -npts/2;

        f = (floatData[i+mid] - min) * scale;

        if (i == (int)(j + 0.5)) {

            // draw a line from the minimum value to the max
            a.setX(x_pix);
            a.setY(hf-accm);
            b.setX(x_pix);
            b.setY(hf-(acc));
            qp.drawLine(a, b);
            //a = b;

            acc = 0.0;
            accm = INFINITY;
            j += step;
            k += 1.0;
            x_pix++;
        }
        //acc += f;
        if (f > acc) acc = f;
        if (f < accm) accm = f;
    }
    //std::cout << std::endl;
    qp.end();
    QImage * old = mGraph;
    mGraph = new_img;
    delete old;
}

void graphPainter::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);

    if (!mGraph)
        return;
    static QPainter qp;

    qp.begin(this);
    scene()->setSceneRect(QRect( QPoint(0,0),mGraph->size() ));
    scene()->addPixmap(QPixmap::fromImage(*mGraph));
    scene()->render(&qp);
    qp.end();

}

void graphPainter::setDataServer(dataServer * aServer)
{
    mDataServer = aServer;
}

void graphPainter::updateGraph()
{
    //std::cout << "updateGraph" << std::endl;
    update();
    //setUpdatesEnabled(true);
    //repaint();
    //QCoreApplication::processEvents(/*QEventLoop::ExcludeSocketNotifiers*/);
}
