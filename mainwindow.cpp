
// Copywrite 2021 James Hemsing
// GPLv2 License

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <iostream>
#include "graphpainter.h"
#include "dataserver.h"
#include <QtMath>
#include <QSplitter>
#include <QTime>
#include <Qt3DRender/QMesh>

// https://doc.qt.io/qt-5/qtdatavisualization-surface-example.html
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QObject::connect(ui->actionStart_Server, SIGNAL(triggered()), this, SLOT(startServer()));
    QObject::connect(ui->actionStop_Server , SIGNAL(triggered()), this, SLOT(stopServer()) );
    QObject::connect(ui->actionUpdate_3D_surface , SIGNAL(triggered()), this, SLOT(toggleSurfaceUpdate()) );

    QSplitter * tSplit = new QSplitter(this);
    tSplit->setOrientation(Qt::Vertical);
    tSplit->setHandleWidth(8);

    ui->centralwidget->layout()->addWidget(tSplit);

    mGraphPainter = new graphPainter();
    mGraphPainter->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum);
    mGraphPainter->setMaximumHeight(256);
    mGraphPainter->setMinimumHeight(64);
    mDataServer = new dataServer(this, mGraphPainter);

    mGraphPainter->setDataServer(mDataServer);

    //ui->horizontalLayout->layout()->addWidget(mGraphPainter);
    tSplit->addWidget(mGraphPainter);

    connect(mDataServer, &dataServer::updateGraph, mGraphPainter, &graphPainter::updateGraph);
    connect(mDataServer, &dataServer::updateGraph, this, &MainWindow::updateSurface);

    mSurface = new QtDataVisualization::Q3DSurface();
    //mSurface->axisY()->setRange(0.0, 40.0);
    mSurface->setShadowQuality(QAbstract3DGraph::ShadowQualityNone);

    QWidget * container = QWidget::createWindowContainer(mSurface);

    //ui->horizontalLayout->layout()->addWidget(container);
    tSplit->addWidget(container);

    mProxy = new QSurfaceDataProxy();
    //QImage tHeightMap (":/heightmaps/res/mountains.png");
    //mProxy = new QHeightMapSurfaceDataProxy(tHeightMap);
    mSeries = new QSurface3DSeries(mProxy);
    mSeries->setFlatShadingEnabled(true);
    mSurface->addSeries(mSeries);

    QValue3DAxisFormatter * tFormat = new QValue3DAxisFormatter();
    QValue3DAxis * tAxis = new QValue3DAxis();
    tAxis->setReversed(true);
    tAxis->setFormatter(tFormat);
    mSurface->setAxisZ(tAxis);

    QLinearGradient gr;
    gr.setColorAt(0.0, Qt::blue);
    gr.setColorAt(0.5, Qt::red);
    gr.setColorAt(1.0, Qt::yellow);
    mSeries->setBaseGradient(gr);
    mSeries->setColorStyle(Q3DTheme::ColorStyleRangeGradient);

  //Q3DTheme * tTheme = new Q3DTheme(Q3DTheme::ThemeRetro);
  //mSurface->setActiveTheme(tTheme);

    //mSeries->setDrawMode((QSurface3DSeries::DrawFlags)QSurface3DSeries::DrawWireframe | QSurface3DSeries::DrawSurface);
    mSeries->setDrawMode((QSurface3DSeries::DrawFlags) QSurface3DSeries::DrawSurface);
    mSeries->setMeshSmooth(false);
    //QtDataVisualization::QAbstract3DSeries::Mesh mesh = mSeries->mesh();

    mNumSpectra = 0;
    mArray = nullptr;
    mSurfaceUpdateEnable = true;

}

MainWindow::~MainWindow()
{
    delete ui;
    delete mDataServer;
}

void MainWindow::startServer()
{
    ui->plainTextEdit->appendPlainText("Starting server");
    mDataServer->start();
}

void MainWindow::stopServer()
{
    int res = mDataServer->stop();
    ui->plainTextEdit->appendPlainText("Server stopped with result: " + QString::number(res));
}

void MainWindow::printConsole(QString tStr)
{
    ui->plainTextEdit->appendPlainText(tStr);
}

void MainWindow::clearSurface()
{
   if (mArray) {
       mArray->clear();
       mArray = nullptr;
   }
   mNumSpectra = 0;
}

void MainWindow::toggleSurfaceUpdate()
{
    mSurfaceUpdateEnable = ui->actionUpdate_3D_surface->isChecked();
}

void MainWindow::updateSurface(float * aSpectrum)
{
    if (!mSurfaceUpdateEnable)
        return;
    float v;
    uint32_t npts = mDataServer->getNumPoints();
    QSurfaceDataRow * tRow = new QSurfaceDataRow(npts);

    const int phase_split = 1;
    int speed = 1000;
    int mid;
    unsigned int j;
    for (j = 0; j < npts; j++) {
        // reorder the points so that center frequency is in the middle
        if (phase_split) {
            if (j < npts/2)
              mid = npts/2;
            else
              mid = -npts/2;
        } else {
            mid = 0;
        }

        v = aSpectrum[j+mid];

        QVector3D tv (j, v, (speed*mNumSpectra));
        (*tRow)[j].setPosition(tv);

        // square each peak, hurts performance
        //(*tRow)[2*j].setPosition(tv);
        //(*tRow)[2*j+1].setPosition(rv);


        //std::cout << std::to_string(tv.x()) << ","
        //          << std::to_string(tv.y()) << ","
        //          << std::to_string(tv.z()) << std::endl;
    }


    mNumSpectra++;

    if (mNumSpectra >= 10) {
        mProxy->removeRows(0,1);
    }

    if (mArray == nullptr) {
        mArray = new QSurfaceDataArray;
        *mArray << tRow;
        mProxy->resetArray(mArray);
    } else {
        mProxy->addRow(tRow);
    }
}
