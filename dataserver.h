// Copywrite 2021 James Hemsing
// GPLv2 License

#ifndef DATASERVER_H
#define DATASERVER_H

#include <QThread>

//forward declaration
class QTcpServer;
class QTcpSocket;
class QDataStream;
class graphPainter;

class dataServer : public QThread
{
    Q_OBJECT
public:
    explicit dataServer(QObject *parent = nullptr, graphPainter * aPainter = nullptr);

    void init(uint32_t npts);
    float * getFloatData();

    //void start();

    int stop();

    unsigned int getNumPoints() { return mNumPoints; };
signals:
    void writeText(QString);
    void updateGraph(float *);
    void clearSurface();

private slots:
    void getData();
private:
    int mStatus;
    QTcpServer * mTcpServer;
    QTcpSocket * mClientSocket;
    graphPainter * mPainter;
    bool mDebug;
    unsigned int mNumPoints;
    bool mServerEnable;
protected:
    void run() override;
    float * mFloatData;
    uint32_t * mReversedIndices;

};

#endif // DATASERVER_H
