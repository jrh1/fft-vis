#include "timer_thread.h"
#include <QRandomGenerator>
#include <QApplication>
#include "mainwindow.h"

timer_thread::timer_thread(MainWindow * aWindow)
{
    npts = 1024;

    data = (int *) malloc (sizeof(int)*npts);

    mWindow = aWindow;
}

void timer_thread::run()
{
    int i, y;
    while (1) {
        for (i = 0; i < npts; i++) {
            y = QRandomGenerator::global()->bounded(200);
            data[i] = y;
        }

        //mWindow->update_data(data);
        emit send_data(data);
        QApplication::processEvents();
        msleep(100);
    }
}

