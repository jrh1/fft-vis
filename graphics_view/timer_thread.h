#ifndef TIMER_THREAD_H
#define TIMER_THREAD_H

#include <QObject>
#include <QThread>

class MainWindow;
class timer_thread : public QThread
{
    Q_OBJECT

public:
    timer_thread(MainWindow *);

signals:
    void send_data(int * data);
protected:
    void run() override;
private:
    int * data;
    int npts;

    MainWindow * mWindow;
};

#endif // TIMER_THREAD_H
