#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class timer_thread;
class graphics_item;
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    int * get_data();
    void update_data(int *);
private:
    Ui::MainWindow *ui;
    graphics_item * square;
    timer_thread * m_timer;
    int * data;
};
#endif // MAINWINDOW_H
