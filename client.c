
// Copywrite 2021 James Hemsing
// GPLv2 License

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <strings.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <math.h>

#define KSIZE 256
#define NPTS 16384
int write_data(int sockfd)
{
	const int size = NPTS;
	static int off = NPTS/2;
	int dir = rand()%3;
	int jump = rand()%200;
 	//int jump = 1;	
       	if (dir == 1 || off <= 0)
		off += jump;
	else if (dir == 2 || off >=(NPTS-KSIZE))
		off -= jump;

	int i;
	float f = 1.0;
	float f_arr [size];
	
	float kern [KSIZE];
	kern[0] = 1;
	kern[1] = 1;
	for (i = 1; i < KSIZE; i++) {
		if (i <= KSIZE/2)
			kern[i] = kern[i-1] + kern[i-2];
		else
			kern[i] = kern[KSIZE-i];
		//printf("%f, ", kern[i]);
	}
	//printf("\n");

	// populate array floats
	for (i = 0; i < size; i++) {	
		//f_arr[i] = (1+sin(2*M_PI*i / p)) * h;
		if (i >=off && i < off + KSIZE) {
			f_arr[i] = kern[i - off];
		} else {
			f_arr[i] = 0.1;
		}
	}

	//printf("size of float array: %lu bytes \n", sizeof(f_arr) );

	int ret = write(sockfd, f_arr, sizeof(f_arr));

	//printf("array written \n");
	return ret;
}

int main (int argc, char ** argv)
{
	char ip_addr [128];
	if (argc > 0) {
		strcpy(ip_addr, argv[1]);
	} else {
		strcpy(ip_addr, "127.0.0.1");
	}

	time_t t = time(NULL);
	srand(t);

	int ret;
	int sockfd, connfd;
	struct sockaddr_in serv_addr;
	while (1) {

		sockfd = socket(AF_INET, SOCK_STREAM, 0);
		if (sockfd < 0) {
			fprintf(stderr, "Failed to create socket with %d. Exiting.\n", sockfd);
			return sockfd;
		}

		serv_addr = (struct sockaddr_in){0};

		serv_addr.sin_family = AF_INET;
		serv_addr.sin_addr.s_addr = inet_addr(ip_addr);
		serv_addr.sin_port = htons(9000);

		connfd = connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr));
		if (connfd != 0) {
			fprintf(stderr, "server connection failed with %d. Exiting\n", connfd);
			return connfd;
		}

		ret = write_data(sockfd);	
		if (ret < sizeof(float)*NPTS)
			fprintf(stdout, "write return %d \n", ret);

		close(sockfd);
		usleep(1000000);
	}



	return 0;
}
